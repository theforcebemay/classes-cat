#-*-coding:utf8;-*-
#qpy:2
#qpy:console

import time

class Cat():
    def __init__(self,name,hungry,room):
        self.name = name
        self.hungry = hungry
        self.room = room

    def tick(self):
        self.hungry -= 1

    def shitonfloor(self):
        self.room -= 1


class Homeless():
    def __init__(self, name, cats):
        self.name = name
        self.cats = cats

    def clean(self):
        for c in self.cats:
            c.hungry += 40

    def cleantime(self):
        for c in self.cats:
            if c.room < 10:
                self.clean()

    def feed(self):
        for c in self.cats:
            c.hungry += 10

    def feedtime(self):
        for c in self.cats:
            if c.hungry < 10:
                self.feed()

cat_igor = Cat('Boris', 50, 50)
cat_pasha = Cat('Pasha', 37, 39)

cats = [cat_igor, cat_pasha]
man = Homeless('Igor', cats)
###############################
while True:
    for c in man.cats:
        c.tick()
        c.shitonfloor()
        man.cleantime()
        man.feedtime()
    for c in cats:
        print '%s осталось %s секунд и %s секунд' % (c.name, c.hungry, c.room)
        time.sleep(1)